﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TextAnalysis
{
	static class SentencesParserTask
	{
		public static readonly string[] StopWords =
		{
			"the", "and", "to", "a", "of", "in", "on", "at", "that",
			"as", "but", "with", "out", "for", "up", "one", "from", "into"
		};

		public static readonly char[] PunctuationMarks =
		{
			'.', '!', '?', ';', ':', '(', ')'
		};

		public static List<List<string>> ParseSentences(string text)
		{
			return text.Split(PunctuationMarks)
				.Where(sentense => !string.IsNullOrEmpty(sentense))
				.Select(sentense => Regex.Split(sentense, "[^a-zA-Z']")
					.Select(word => word.ToLower())
					.Where(word => !string.IsNullOrEmpty(word) && !StopWords.Contains(word))
					.ToList())
				.Where(list => list.Count > 0)
				.ToList();
		}
	}
}


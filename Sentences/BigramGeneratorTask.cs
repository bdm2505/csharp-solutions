﻿using System.Collections.Generic;
using System.Text;

namespace TextAnalysis
{
	static class BigramGeneratorTask
	{
		public static string ContinuePhraseWithBigramms(
			Dictionary<string, string> mostFrequentNextWords, 
			string phraseBeginning, 
			int phraseWordsCount)
		{
			var phrase = phraseBeginning;
			var result = new StringBuilder(phrase);
			for (var i = 1; i < phraseWordsCount; i++)
			{
				if (!mostFrequentNextWords.ContainsKey(phrase))
					continue;
				phrase = mostFrequentNextWords[phrase];
				result.Append(" ").Append(phrase);
			}
			return result.ToString();
		}
	}
}
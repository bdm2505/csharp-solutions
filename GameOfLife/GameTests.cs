﻿using NUnit.Framework;

namespace GameOfLife
{
    [TestFixture]
    public class GameTests
    {
        public static void CountEqual(bool[,] field, int x, int y, int res)
        {
            Assert.AreEqual(Game.Count(field, x, y), res);
        }

        public static void NextEqual(bool[,] field, bool[,] actual)
        {
            Assert.AreEqual(Game.NextStep(field), actual);
        }
        [Test]
        public void CountTest()
        {
            CountEqual( new[,] { {false, false},  {false, true}}, 0, 0, 1);
            CountEqual( new[,] { {false, false},  {true, true}}, 0, 0, 2);
            CountEqual( new[,] { {false, false},  {true, true}}, 1, 0, 1);
            CountEqual( new[,] { {true, true, true}, {true, true, true}}, 0, 0, 3 );
        }
        [Test]
        public void DeathTest()
        {
            NextEqual(new[,] { {false, false},  {false, true}},
                      new[,] { {false, false},  {false, false}});
        }
        
    }
}